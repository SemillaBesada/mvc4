import { Repository } from 'typeorm/repository/Repository';
import { IOperationBehavior } from '../operation-behavior.interface';

import { IModel } from '../../model/model.interface';
import { IOperation, IResult } from '../../operation/operation.interface';
import { IDatabase } from '../../database/database.interface';
import { ResourceType } from '../../model/entity/resource-type.model';


export abstract class AbstractResourceBehavior<T extends IModel> implements IOperationBehavior {

  constructor(protected repo: Repository<T>, protected resourceType: ResourceType, protected db: IDatabase) { }

  abstract execute(operation: IOperation): Promise<IResult>;

}