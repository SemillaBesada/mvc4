import { IJwtUserInfo } from '../../operation/jwt.operation';
import { User } from '../../model/entity/user.model';


export interface IAuthenticatable {
  authenticate(tokenBearer: string, subject?: string): Promise<User>;
}