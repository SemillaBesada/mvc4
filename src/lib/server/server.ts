import * as http from 'http';
import * as express from 'express';
import * as SocketIO from 'socket.io';

import { IApiServer } from './server.interface';
import { IApiClient } from '../client/client.interface';
import { IApiSocketClient } from './socket-client.interface';
import { ApiSocketClient } from './socket-client';


export class ApiServer implements IApiServer {

  socketClient: IApiSocketClient;

  constructor(
    private api: IApiClient,
    private expressApp: express.Application,
    private webServer: http.Server,
    private webSocket: SocketIO.Server,
  ) {

    this.socketClient = new ApiSocketClient(api, webSocket);

    webServer.on('error', (error: any) =>
      this.onWebServerError(error));

    expressApp.get('/token', (req, res, next) => {

      console.log('in /token middleware....');

      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
      next();
    }, this.onTokenRequest.bind(this));

    webSocket.on('connection', (socket) =>
      this.onSocketConnection(socket));
    webSocket.on('disconnect', (reason: any) =>
      this.onSocketDisconnect(reason));
    webSocket.on('error', (error: any) =>
      this.onSocketError(error));
  }

  run(): void {
    this.expressApp.get('/', (req, res) => {
      res.send('helo wodlrs');
    });
    this.webServer.listen(8080, () => this.onWebServerUp());
  }

  logMessage(text: string) {
    console.log(`[API SERVER] ${text}`);
  }

  onTokenRequest(req: express.Request, res: express.Response): void {
    console.log('in /token req handler....');
    this.api.generateToken('anonymous').then(
      token => res.send({ token })
    );
  }

  onWebServerUp() {
    this.logMessage(`webserver up and running on port: ${this.webServer.address().port}`);
    this.logMessage(`Core Info:\n${this.api.coreInfo}`);
    this.api.generateToken('api').then(apiToken => this.logMessage(`admin token:\n${apiToken}`));
  }

  onWebServerError(error: any) {
    this.logMessage(`webserver error: ${error.message || error}`);
  }

  onSocketConnection(socket: SocketIO.Socket) {
    this.logMessage(`socket connection: ${socket.id}`);
  }

  onSocketDisconnect(reason: any) {
    this.logMessage(`socket disconnected: ${reason.message || reason}`);
  }

  onSocketError(error: any) {
    this.logMessage(`socket error: ${error.message || error}`);
  }

}

