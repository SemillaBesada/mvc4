import { IRemoteController } from './remote-controller.interface';
import { IResourceCreator, IResourceReader, IResourceFinder, IResourceUpdater, IResourceRemover, IResourceFindOner } from '../behavior/resource/resource-behavior.interface';


export interface IResourceController<T> extends
  IRemoteController,
  IResourceCreator<T>,
  IResourceReader<T>,
  IResourceUpdater<T>,
  IResourceRemover,
  IResourceFinder<T>,
  IResourceFindOner<T> { }
