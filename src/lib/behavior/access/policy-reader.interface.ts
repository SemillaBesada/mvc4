import { Policy } from '../../model/entity/policy.model';
import { Operation } from '../../model/entity/operation.model';
import { Role } from '../../model/entity/role.model';
import { ResourceType } from '../../model/entity/resource-type.model';


export interface IPolicyReader {
  getPolicies(roles: Role[], resourceType: ResourceType, operation: Operation): Promise<Policy[]>;
}