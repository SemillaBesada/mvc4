import { Repository } from 'typeorm/repository/Repository';

import { AbstractControllerFactory } from './abstract-controller.factory';

import { IModel } from '../model/model.interface';
import { IDatabase } from '../database/database.interface';
import { ResourceType } from '../model/entity/resource-type.model';

import { IResourceController } from './resource-controller.interface';
import { ResourceController } from './resource.controller';
import { ResourceBehaviorMap } from '../behavior/resource/resource-behavior.interface';

import * as behaviors from '../behavior/resource/resource-behavior';



export class DefaultResourceControllerFactory<T extends IModel> implements AbstractControllerFactory {

  constructor(private repo: Repository<T>, private db: IDatabase) { }

  create(resourceType: ResourceType): IResourceController<T> {

    const repo = this.repo;
    const db = this.db;

    const resourceBehaviors: ResourceBehaviorMap<T> = {
      create: new behaviors.DefaultCreateResourceBehavior(repo, resourceType, db),
      read: new behaviors.DefaultReadResourceBehavior(repo, resourceType, db),
      update: new behaviors.DefaultUpdateResourceBehavior(repo, resourceType, db),
      remove: new behaviors.DefaultRemoveResourceBehavior(repo, resourceType, db),
      find: new behaviors.DefaultFindBehavior(repo, resourceType, db),
      findOne: new behaviors.DefaultFindOneBehavior(repo, resourceType, db)
    };

    return new ResourceController(resourceBehaviors);
  }
}