import { Repository } from 'typeorm/repository/Repository';

import { IOperationBehavior } from '../operation-behavior.interface';
import { IOperation, IResult } from '../../operation/operation.interface';
import { IJwtUserInfo } from '../../operation/jwt.operation';
import { IAuthenticatable } from './authenticatable.interface';
import { ITokenDecoder } from '../jwt/token-decoder.interface';
import { IResourceController } from '../../controller/resource-controller.interface';
import { IRegisterUser } from './registerable.interface';

import { User } from '../../model/entity/user.model';

import * as authOps from '../../operation/auth.operation';


export class DefaultRegisterBehavior implements IOperationBehavior, IRegisterUser, ITokenDecoder {

  constructor(
    private readonly tokenDecoder: ITokenDecoder,
    private readonly users: IResourceController<User>
  ) { }

  execute(operation: authOps.Register): Promise<IResult> {
    return this.registerUser(operation.payload.tokenBearer).then(
      user => new authOps.RegisterSuccess(user),
      error => new authOps.RegisterFailed(error.message || error)
    );
  }

  registerUser(tokenBearer: string): Promise<User> {
    return this.decodeToken(tokenBearer).then(userInfo =>
      this.users.create({ sub: userInfo.sub, email: userInfo.email })
    );
  }

  decodeToken(tokenBearer: string, subject?: string | undefined): Promise<IJwtUserInfo> {
    return this.tokenDecoder.decodeToken(tokenBearer);
  }
}

export class DefaultLoginBehavior implements IOperationBehavior, IAuthenticatable, ITokenDecoder {

  constructor(
    private readonly tokenDecoder: ITokenDecoder,
    private readonly userController: IResourceController<User>
  ) { }

  execute(operation: authOps.Login): Promise<IResult> {
    return this.authenticate(
      operation.payload.tokenBearer,
      operation.payload.subject  //if logging in with self generated token
    ).then(
      user => new authOps.LoginSuccess(user),
      error => new authOps.LoginFailed(error.message || error));
  }

  authenticate(tokenBearer: string, subject?: string): Promise<User> {
    return this.decodeToken(tokenBearer, subject).then(userInfo =>
      this.userController.findOne({ sub: userInfo.sub }, ['roles'])
    );
  }

  decodeToken(tokenBearer: string, subject?: string | undefined): Promise<IJwtUserInfo> {
    return this.tokenDecoder.decodeToken(tokenBearer, subject);
  }
}

export class DefaultLogoutBehavior implements IOperationBehavior {
  execute(operation: authOps.Logout): Promise<IResult> {
    return Promise.resolve(new authOps.LogoutSuccess());
  }
}
