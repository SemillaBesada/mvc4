import * as fs from 'fs';

import { IdentityMap } from '../behavior/jwt/identity-map.interface';
import { Role } from '../model/entity/role.model';

export interface IClientIdentityMapFactory {
  createIdentityMap(): IdentityMap;
}

export class DefaultClientIdentityMapFactory implements IClientIdentityMapFactory {

  createIdentityMap(): IdentityMap {
    const privateApiKey = fs.readFileSync(`${__dirname}/../../../ssl/api_private_key.pem`);
    const privateAnonymousKey = fs.readFileSync(`${__dirname}/../../../ssl/anonymous_private_key.pem`);

    const publicApiKey = fs.readFileSync(`${__dirname}/../../../ssl/api_public_key.pem`);
    const publicAnonymousKey = fs.readFileSync(`${__dirname}/../../../ssl/anonymous_public_key.pem`);

    const adminRole = new Role();
    adminRole.id = 1;
    adminRole.name = 'admin'

    return {
      api: {
        privateKey: privateApiKey,
        publicKey: publicApiKey,
        roles: [adminRole]
      },
      anonymous: {
        privateKey: privateAnonymousKey,
        publicKey: publicAnonymousKey,
        roles: []
      }
    };
  };

}