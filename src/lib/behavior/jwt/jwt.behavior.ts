import * as request from 'request';
import * as jsonWebtoken from 'jsonwebtoken';

import { IOperationBehavior } from '../operation-behavior.interface';
import { IOperation, IResult } from '../../operation/operation.interface';
import { IJwtUserInfo, VerifyJwtSuccess } from '../../operation/jwt.operation';
import { ITokenGenerator } from './token-generator.interface';
import { ITokenDecoder } from './token-decoder.interface';
import { IdentityMap } from './identity-map.interface';

import * as jwtOps from '../../operation/jwt.operation';


const audience = 'http://localhost:3030/api';
const issuer = "https://semillabesada.eu.auth0.com/";


export class DefaultGenerateJwtBehavior implements IOperationBehavior, ITokenGenerator {

  public readonly claimNamespace = 'https://semilla-besada.com/api';

  constructor(private identities: IdentityMap) { }

  execute(operation: jwtOps.GenerateJwtOperation): Promise<IResult> {
    if (operation.payload.sub in this.identities)
      return this.generateToken(operation.payload.sub).then(
        tokenBearer => new jwtOps.GenerateJwtSuccess(tokenBearer),
        error => new jwtOps.GenerateJwtFailed(error.message || error)
      );
    else return Promise.resolve(
      new jwtOps.GenerateJwtFailed(`subject ${operation.payload.sub} has no private key`)
    );
  }

  generateToken(subject: string): Promise<string> {
    return new Promise((resolve, reject) => jsonWebtoken.sign({
      [`${this.claimNamespace}/roles`]: this.identities[subject].roles
    }, this.identities[subject].privateKey, {
        algorithm: 'RS256',
        audience,
        issuer,
        subject,
        expiresIn: '1d'
      }, (err: Error, tokenBearer: string) => {
        if (err) reject(err);
        else resolve(tokenBearer);
      })
    );
  }
}


export class DefaultVerifyJwtBehavior implements IOperationBehavior, ITokenDecoder {

  constructor(private identities: IdentityMap) { }

  execute(operation: jwtOps.VerifyJwtOperation): Promise<IResult> {
    return this.decodeToken(operation.payload.tokenBearer, operation.payload.subject).then(
      userInfo => new jwtOps.VerifyJwtSuccess(userInfo),
      error => new jwtOps.VerifyJwtFailed(error.message || error)
    );
  }

  decodeToken(tokenBearer: string, subject?: string): Promise<IJwtUserInfo> {
    if (subject && subject in this.identities)
      return this.decodeLocalToken(tokenBearer, subject);
    else
      return this.decodeRemoteToken(tokenBearer);
  }

  decodeLocalToken(tokenBearer: string, subject: string): Promise<IJwtUserInfo> {

    return new Promise((resolve, reject) => jsonWebtoken.verify(
      tokenBearer, this.identities[subject].publicKey, {
        algorithms: ['RS256'],
        audience,
        issuer,
        subject,
        maxAge: "1d"
      }, (error: Error, decoded: any) => {
        if (error) reject(new Error(error.message));
        else resolve({
          sub: decoded.sub,
          name: subject === 'api' ? 'Administrator' : 'Tourist',
          email: `${subject}@semilla-besada.com`,
          email_verified: true
        });
      })
    );
  }

  decodeRemoteToken(tokenBearer: string): Promise<IJwtUserInfo> {
    return new Promise((resolve, reject) =>
      request('https://semillabesada.eu.auth0.com/userinfo', {
        json: true,
        headers: { Authorization: `Bearer ${tokenBearer}` }
      }, (error, response, userInfo: IJwtUserInfo) => {
        if (error) reject(new Error(error.message || error));
        if (response.statusCode !== 200)
          reject(new Error(`${response.statusCode}: ${response.statusMessage}`));

        else {
          resolve(userInfo);
        }
      })
    );
  }
}