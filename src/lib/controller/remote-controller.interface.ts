import { IOperationBehavior } from '../behavior/operation-behavior.interface';
import { IOperation, IResult } from '../operation/operation.interface';
import { IController } from './controller.interface';


export interface IRemoteController extends IController {

  behaviorNames: string[];

  setBehavior(name: string, behavior: IOperationBehavior): void;

  execute(operation: IOperation): Promise<IResult>;
  
}