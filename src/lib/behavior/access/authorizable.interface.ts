import { IApiAction } from '../../client/action.interface';
import { User } from '../../model/entity/user.model';


export interface IAuthorizeable {
  authorize(action: IApiAction, user?: User): Promise<boolean>;
}