import { Entity } from 'typeorm';

import { ICoreManager } from './core-manager.interface';
import { CoreHelper } from './resource-decorator';

import { CoreModelName } from '../model/model.interface';
import { OperationName, IOperation } from '../operation/operation.interface';

import { ResourceType } from '../model/entity/resource-type.model';
import { Operation } from '../model/entity/operation.model';
import { Role } from '../model/entity/role.model';
import { User } from '../model/entity/user.model';


export class CoreManager implements ICoreManager {

  public readonly apiUser: User;
  public readonly anonymousUser: User;

  private operationMap: { [name: string]: Readonly<Operation> };
  private rolesMap: { [name: string]: Readonly<Role> };

  get operations(): Operation[] {
    const opMap = this.operationMap;

    return Object.keys(opMap).map(
      key => opMap[key]
    );
  }

  get resourceTypes(): ResourceType[] {
    const typeMap = this.resourceTypeMap;

    return Object.keys(typeMap).map(
      key => typeMap[key]
    );
  }

  get roles(): Role[] {
    const rolesMap = this.rolesMap;

    return Object.keys(rolesMap).map(
      key => rolesMap[key]
    );
  }

  getResourceType(name: string): ResourceType {
    return this.resourceTypeMap[name];
  }

  getOperation(name: string): Operation {
    return this.operationMap[name];
  }

  getRole(name: string): Role {
    return this.rolesMap[name];
  }



  constructor(
    private resourceTypeMap: { [name: string]: Readonly<ResourceType> }
  ) {
    this.operationMap = this.buildOperationMap();
    this.rolesMap = this.buildRolesMap();
    this.apiUser = this.createApiUser();
    this.anonymousUser = this.createAnonymousUser();
  }

  buildOperationMap(): { [name: string]: Readonly<Operation> } {
    return Object.values(OperationName)
      .reduce((acc: { [name: string]: Operation }, opName: string, i) => {

        const operation = new Operation();
        operation.id = i + 1;
        operation.name = opName;

        acc[opName] = operation;

        return acc;
      }, {});
  }

  buildRolesMap(): { [name: string]: Readonly<Role> } {
    return ['admin', 'owner', 'member']
      .reduce((acc: { [name: string]: Role }, roleName: string, i) => {

        const role = new Role();
        role.id = i + 1;
        role.name = roleName;

        acc[roleName] = role;

        return acc;
      }, {});
  }

  createApiUser(): User {
    const user = new User();
    user.id = 1;
    user.sub = 'api';
    user.email = 'api@semilla-besada.com';
    user.roles = [this.getRole('admin')];
    user.isPublic = false;

    return user;
  }

  createAnonymousUser(): User {
    const user = new User();
    user.id = 2;
    user.sub = 'anonymous';
    user.email = 'anonymous@semilla-besada.com';
    user.roles = [];
    user.isPublic = false;

    return user;
  }

}

