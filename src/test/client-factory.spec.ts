import { expect } from 'chai';

import { TestDatabaseFactory } from '../lib/database/database-factory';
import { DefaultApiClientFactory, ApiClientFactory } from '../lib/client/client.factory';
import { IApiClient } from '../lib/client/client.interface';
import { CoreModelName } from '../lib/model/model.interface';
import { OperationName } from '../lib/operation/operation.interface';

import * as ops from '../lib/operation/resource.operation';
import * as authOps from '../lib/operation/auth.operation';

import { Policy } from '../lib/model/entity/policy.model';
import { User } from '../lib/model/entity/user.model';


describe('client factory test', () => {

  const dbFactory = new TestDatabaseFactory();
  const clientFactory: ApiClientFactory = new DefaultApiClientFactory();

  let client: IApiClient;
  let apiToken: string;
  let apiUser: User;


  before('it should create a client', async () => {
    client = await clientFactory.create(dbFactory);
    // console.log(client.coreInfo);
  });

  it('should generate a token for api user', async () => {

    apiToken = await client.generateToken('api');

    expect(apiToken).a('string');
  });

  it('should authenticate with the token', async () => {

    apiUser = await client.authenticate(apiToken, 'api');

    expect(apiUser).instanceof(User);
    expect(apiUser.id).eql(1);
    expect(apiUser.sub).eql('api');
  });

  it('should execute a command', async () => {

    const action = await client.runCommand({
      model: CoreModelName.USER, 
      operation: new ops.Find({}, ['roles'])
    }, apiUser);

    if (action.result.error) throw new Error(action.result.error);

    const allUsers: User[] = action.result.payload.resources;

    expect(allUsers).instanceof(Array);
  });

  it('should add policies', async () => {

    const core = dbFactory.coreMgr;
    const userType = core.getResourceType(CoreModelName.USER);

    const readOp = core.getOperation(OperationName.READ);
    const updateOp = core.getOperation(OperationName.UPDATE);
    const findOp = core.getOperation(OperationName.FIND);
    const findOneOp = core.getOperation(OperationName.FINDONE);

    const owner = core.getRole('owner');
    const admin = core.getRole('admin');
    const member = core.getRole('member');

    await client.addPolicies(admin, userType, [readOp, findOp, findOneOp]);
    await client.addPolicies(member, userType, [readOp, findOp, findOneOp]);

    const action = await client.runCommand({
      model: CoreModelName.POLICY,
      operation: new ops.Find({}, ['role', 'resourceType', 'operation'])
    }, apiUser);

    if (action.result.error) throw new Error(action.result.error);

    const policies: Policy[] = action.result.payload.resources;

    expect(policies).instanceof(Array);
  });

});