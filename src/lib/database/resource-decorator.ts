import { Entity } from 'typeorm';

import { CoreManager } from './core-manager';


export class CoreHelper {

  private static resourceTypeId = 0;

  static registeredTypes: {
    [key: string]: { id: number, name: string; }
  } = {};

  static registerModel(name: string): void {
    CoreHelper.registeredTypes[name] = {
      id: ++CoreHelper.resourceTypeId,
      name
    };
    // console.log(`${name} registered ok!`)
  }

}


export function EntityResource(name: string): Function {

  CoreHelper.registerModel(name);

  return Entity(name);
}