import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';
import { EntityResource } from '../../database/resource-decorator';
import { CoreModelName } from '../model.interface';


@Entity(CoreModelName.RESOURCE_TYPE)
export class ResourceType  {
  
  @PrimaryGeneratedColumn()
  id!: number;

  @Column({ unique: true, type: 'varchar', length: 50 })
  name!: string;

}