import { IApiAction, IApiResult } from './action.interface';
import { IAccessController } from '../controller/access-controller.interface';
import { IAuthController } from '../controller/auth-controller.interface';

import { IAuthenticatable } from '../behavior/auth/authenticatable.interface';
import { IAuthorizeable } from '../behavior/access/authorizable.interface';
import { IRegisterUser } from '../behavior/auth/registerable.interface';
import { ITokenGenerator } from '../behavior/jwt/token-generator.interface';
import { IPolicyReader } from '../behavior/access/policy-reader.interface';
import { IPolicyAdder } from '../behavior/access/policy-adder.interface';
import { ICoreManager } from '../database/core-manager.interface';
import { User } from '../model/entity/user.model';


export interface IApiClient extends IAuthenticatable, IAuthorizeable, IRegisterUser, ITokenGenerator, IPolicyAdder, IPolicyReader {

  apiUser: User;
  anonymousUser: User;

  controllerNames: string[];
  controllerBehaviorNames: { [modelName: string]: string[] };

  runCommand(action: IApiAction, user?: User): Promise<IApiResult>;

  execute(action: IApiAction): Promise<IApiResult>;

  coreInfo: string;
}