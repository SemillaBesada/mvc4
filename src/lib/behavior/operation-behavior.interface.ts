import { IOperation, IResult } from '../operation/operation.interface';


export interface IOperationBehavior {
  execute(operation: IOperation): Promise<IResult>;
}