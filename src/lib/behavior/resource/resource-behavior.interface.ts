import { IOperationBehavior } from '../operation-behavior.interface';
import { BehaviorMap } from '../behavior-map.interface';

import { User } from '../../model/entity/user.model';
import { DeepPartial } from 'typeorm/common/DeepPartial';


export interface IResourceCreator<T> extends IOperationBehavior {
  create(value: DeepPartial<T>, user?: User): Promise<T>;
}

export interface IResourceReader<T> extends IOperationBehavior {
  read(id: number, relations?: string[]): Promise<T>;
}

export interface IResourceUpdater<T> extends IOperationBehavior {
  update(id: number, changes: Partial<T>): Promise<Partial<T>>;
}

export interface IResourceRemover extends IOperationBehavior {
  remove(id: number): Promise<number>;
}

export interface IResourceFinder<T> extends IOperationBehavior {
  find(conditions?: Partial<T>, relations?: string[]): Promise<T[]>;
}

export interface IResourceFindOner<T> extends IOperationBehavior {
  findOne(conditions: Partial<T>, relations?: string[]): Promise<T>;
}

export interface ResourceBehaviorMap<T> extends BehaviorMap {
  create: IResourceCreator<T>,
  read: IResourceReader<T>,
  update: IResourceUpdater<T>,
  remove: IResourceRemover,
  find: IResourceFinder<T>,
  findOne: IResourceFindOner<T>
}