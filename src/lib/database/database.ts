import { Connection, Repository, EntitySubscriberInterface } from 'typeorm';

import { IDatabase } from './database.interface';
import { IModel } from '../model/model.interface';
import { ResourceType } from '../model/entity/resource-type.model';
import { Owner } from '../model/entity/owner.model';
import { ICoreManager } from './core-manager.interface';


export class Database implements IDatabase {

  get coreResourceTypes(): ResourceType[] {
    return this.coreMgr.resourceTypes;
  }

  constructor(
    private connection: Connection,
    public coreMgr: ICoreManager
  ) { }

  getRepository<T>(name: string): Repository<T> {
    return this.connection.getRepository(name);
  }

  addSubscriber(subscriber: EntitySubscriberInterface): void {
    this.connection.subscribers.push(subscriber);
  }
}