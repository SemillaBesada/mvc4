import { IApiClient } from './client.interface';
import { IRemoteController } from '../controller/remote-controller.interface';
import { IApiAction, IApiResult } from './action.interface';

import { IAuthController } from '../controller/auth-controller.interface';
import { IAccessController } from '../controller/access-controller.interface';
import { User } from '../model/entity/user.model';
import { Role } from '../model/entity/role.model';
import { ResourceType } from '../model/entity/resource-type.model';
import { Operation } from '../model/entity/operation.model';
import { Policy } from '../model/entity/policy.model';
import { ICoreManager } from '../database/core-manager.interface';

import * as accessOps from '../operation/access.operation';


export class ApiClient implements IApiClient {

  get coreInfo(): string {
    return this.controllerNames.reduce((acc, name, i) => {
      acc += `* ${name} : [${this.controllers[name].behaviorNames.join(',')}]\n`
      return acc;
    }, '');
  }

  get apiUser(): User {
    return this.auth.apiUser;
  }

  get anonymousUser(): User {
    return this.auth.anonymousUser;
  }

  get controllerNames(): string[] {
    return Object.keys(this.controllers);
  }

  get controllerBehaviorNames(): { [modelName: string]: string[] } {
    return this.controllerNames.reduce(
      (acc: { [modelName: string]: string[] }, modelName) => {
        acc[modelName] = this.controllers[modelName].behaviorNames;
        return acc
      }, {}
    );
  }

  constructor(
    private readonly controllers: { [key: string]: IRemoteController },
    public readonly auth: IAuthController,
    public readonly access: IAccessController
  ) { }

  runCommand(action: IApiAction, user?: User): Promise<IApiResult> {
    return this.authorize(action, user).then(
      isAuthorized => isAuthorized ?
        this.execute(action, user) : ({
          model: action.model,
          result: new accessOps.AuthorizeFailed('Not Authorized'),
          error: 'Authorization failed'
        }),
      error => this.createError(action, error.message || error)
    );
  }

  generateToken(subject: string): Promise<string> {
    return this.auth.generateToken(subject);
  }

  registerUser(tokenBearer: string): Promise<User> {
    return this.auth.registerUser(tokenBearer);
  }

  authenticate(tokenBearer: string, subject?: string): Promise<User> {
    return this.auth.authenticate(tokenBearer, subject);
  }

  authorize(action: IApiAction, user?: User): Promise<boolean> {
    return this.access.authorize(action, user);
  }

  addPolicies(role: Role, resourceType: ResourceType, operations: Operation[]): Promise<Policy[]> {
    return this.access.addPolicies(role, resourceType, operations);
  }

  getPolicies(roles: Role[], resourceType: ResourceType, operation: Operation): Promise<Policy[]> {
    return this.access.getPolicies(roles, resourceType, operation);
  }

  execute(action: IApiAction, user?: User): Promise<IApiResult> {

    if (action.model in this.controllers) {
      return this.controllers[action.model].execute(action.operation).then(
        result => ({ model: action.model, result }),
        error => this.createError(action, error.message || error)
      );
    }

    return Promise.resolve(this.createError(action, 'invalid model'));
  }

  createError(action: IApiAction, error: string): IApiResult {
    return ({ model: action.model, error, result: { type: 'invalid' } });
  }

}