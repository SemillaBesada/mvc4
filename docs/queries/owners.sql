SELECT user.sub as owner, resource_type.name, resourceId
FROM test.owner
INNER JOIN resource_type on owner.typeId = resource_type.id
INNER JOIN user on owner.userId = user.id