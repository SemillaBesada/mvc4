import { IApiClient } from "../client/client.interface";


export interface IApiSocketClient {

  apiClient: IApiClient;

  onLogin(socketId: string, tokenBearer: string, subject?: string): void;
  onRegister(socketId: string, tokenBearer: string): void;
  onLogout(socketId: string, subject: string): void;

  onMessage(socketId: string, message: any): void;
  onError(socketId: string, error: any): void;

  onConnect(socket: SocketIO.Socket): void;
  onDisconnect(reason?: any): void;
}