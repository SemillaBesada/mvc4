import { Owner } from './entity/owner.model';
import { ResourceType } from './entity/resource-type.model';


export enum CoreModelName {
  USER = 'user',
  ROLE = 'role',
  POLICY = 'policy',
  OPERATION = 'operation',
  RESOURCE_TYPE = 'resource_type',
}

export interface IModel {

  id: number;
  resourceType: ResourceType;

}