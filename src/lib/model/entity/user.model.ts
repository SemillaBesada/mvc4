import { Column, OneToOne, OneToMany, JoinTable, ManyToMany } from "typeorm";

import { EntityResource } from '../../database/resource-decorator';
import { CoreModelName } from '../model.interface';
import { AbstractResourceModel } from '../abstract-resource.model';
import { Role } from './role.model';


@EntityResource(CoreModelName.USER)
export class User extends AbstractResourceModel {

  @Column({ unique: true, length: 50 })
  sub!: string;  

  @Column()
  email!: string;

  @Column({ default: true })
  isPublic!: boolean;

  @ManyToMany(type => Role, role => role.id)
  @JoinTable()
  roles!: Role[];

}