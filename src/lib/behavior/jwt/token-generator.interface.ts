export interface ITokenGenerator {
  generateToken(subject: string): Promise<string>;
}