import { AbstractControllerFactory } from './abstract-controller.factory';
import { IdentityMap } from '../behavior/jwt/identity-map.interface';
import { IRemoteController } from './remote-controller.interface';
import { IAuthController } from './auth-controller.interface';
import { IResourceController } from './resource-controller.interface';

import { AuthController } from './auth.controller';
import { User } from '../model/entity/user.model';

import * as authBehaviors from '../behavior/auth/auth.behavior';
import * as jwtBehaviors from '../behavior/jwt/jwt.behavior';


export class DefaultAuthControllerFactory {

  constructor(
    private readonly apiUser: User,
    private readonly anonymousUser: User,
    private identities: IdentityMap,
    private userController: IResourceController<User>) {
  }

  async create(operations?: string[] | undefined): Promise<IAuthController> {

    const tokenDecoder = new jwtBehaviors.DefaultVerifyJwtBehavior(this.identities);

    return new AuthController(
      this.apiUser,
      this.anonymousUser,
      new jwtBehaviors.DefaultGenerateJwtBehavior(this.identities),
      new authBehaviors.DefaultRegisterBehavior(tokenDecoder, this.userController),
      new authBehaviors.DefaultLoginBehavior(tokenDecoder, this.userController),
      new authBehaviors.DefaultLogoutBehavior()
    );
  }

}