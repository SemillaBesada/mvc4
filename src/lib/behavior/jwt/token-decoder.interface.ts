import { IJwtUserInfo } from '../../operation/jwt.operation';


export interface ITokenDecoder {
  decodeToken(tokenBearer: string, subject?: string): Promise<IJwtUserInfo>;
}