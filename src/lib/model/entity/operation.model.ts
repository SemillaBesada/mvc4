import { Column } from 'typeorm';

import { EntityResource } from '../../database/resource-decorator';
import { CoreModelName } from '../model.interface';
import { AbstractResourceModel } from '../abstract-resource.model';


@EntityResource(CoreModelName.OPERATION)
export class Operation extends AbstractResourceModel {

  @Column({ length: 25, unique: true })
  name!: string;

}