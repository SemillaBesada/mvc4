import { Column } from 'typeorm';

import { EntityResource } from '../../database/resource-decorator';
import { CoreModelName } from '../model.interface';
import { AbstractResourceModel } from '../abstract-resource.model';


@EntityResource(CoreModelName.ROLE)
export class Role extends AbstractResourceModel {

  @Column({ unique: true, type: 'varchar', length: 50 })
  name!: string;

}