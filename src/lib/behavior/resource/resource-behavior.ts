import { Repository } from 'typeorm/repository/Repository';
import { DeepPartial } from 'typeorm/common/DeepPartial';

import { IDatabase } from '../../database/database.interface';
import { IModel, CoreModelName } from '../../model/model.interface';
import { AbstractResourceBehavior } from './abstract-resource-behavior';

import {
  IResourceCreator, IResourceReader, IResourceUpdater,
  IResourceRemover, IResourceFinder, IResourceFindOner
} from './resource-behavior.interface';

import { Owner } from '../../model/entity/owner.model';
import { User } from '../../model/entity/user.model';
import { ResourceType } from '../../model/entity/resource-type.model';

import * as resourceOps from '../../operation/resource.operation';



export class DefaultCreateResourceBehavior<T extends IModel> extends AbstractResourceBehavior<T> implements IResourceCreator<T> {

  private ownerRepo: Repository<Owner>;
  private apiUser = new User();

  constructor(protected repo: Repository<T>, resourceType: ResourceType, db: IDatabase) {
    super(repo, resourceType, db);
    this.apiUser.id = 1;
    this.ownerRepo = db.getRepository(Owner);
  }

  async execute(operation: resourceOps.Create<T>): Promise<resourceOps.CreateSuccess<T> | resourceOps.CreateFailed> {
    return this.create(operation.payload.resource, operation.payload.user).then(
      resource => new resourceOps.CreateSuccess<T>(resource),
      error => new resourceOps.CreateFailed(error)
    );
  }

  create(value: DeepPartial<T>, user?: User): Promise<T> {

    const owner = new Owner();
    owner.type = this.resourceType;

    return this.repo.manager.transaction(async mgr => {

      const resource = await mgr.save(this.repo.create(value));

      owner.resourceId = resource.id;
      owner.user = user && user.id > 2 ?
        user : this.apiUser;

      return mgr.save(owner).then(() => resource);
    });
  }
}

export class DefaultReadResourceBehavior<T extends IModel> extends AbstractResourceBehavior<T> implements IResourceReader<T> {
  read(id: number, relations?: string[]): Promise<T> {
    return this.repo.findOneById(id, { cache: true, relations }).then(
      resource => {
        if (resource) return resource;
        else throw new Error(`${this.resourceType.name} not found`);
      }
    );
  }
  execute(operation: resourceOps.Read<T>): Promise<resourceOps.ReadSuccess<T> | resourceOps.ReadFailed<T>> {
    return this.read(operation.payload.id, operation.payload.relations).then(
      resource => new resourceOps.ReadSuccess<T>(resource),
      error => new resourceOps.ReadFailed(error.message)
    );
  }
}

export class DefaultUpdateResourceBehavior<T extends IModel> extends AbstractResourceBehavior<T> implements IResourceUpdater<T> {
  update(id: number, changes: Partial<T>): Promise<Partial<T>> {
    return this.repo.findOneById(id).then(
      id => this.repo.updateById(id, changes as any).then(() => ({ id, ...changes as any }))
    );
  }
  execute(operation: resourceOps.Update<T>): Promise<resourceOps.UpdateSuccess<T> | resourceOps.UpdateFailed<T>> {
    return this.update(operation.payload.id, operation.payload.changes).then(
      resource => new resourceOps.UpdateSuccess<T>(resource),
      error => new resourceOps.UpdateFailed<T>(error)
    );
  }
}

export class DefaultRemoveResourceBehavior<T extends IModel> extends AbstractResourceBehavior<T> implements IResourceRemover {
  remove(id: number): Promise<number> {
    return this.repo.removeById(id).then(() => id);
  }
  execute(operation: resourceOps.Remove<T>): Promise<resourceOps.RemoveSuccess<T> | resourceOps.RemoveFailed<T>> {
    return this.remove(operation.payload.id).then(
      () => new resourceOps.RemoveSuccess<T>(operation.payload.id),
      error => new resourceOps.RemoveFailed(error.message || error)
    );
  }
}

export class DefaultFindBehavior<T extends IModel> extends AbstractResourceBehavior<T> implements IResourceFinder<T> {
  find(conditions?: Partial<T> | undefined, relations?: string[] | undefined): Promise<T[]> {
    return this.repo.find({ where: conditions || {}, relations, cache: true });
  }
  execute(operation: resourceOps.Find<T>): Promise<resourceOps.FindSuccess<T> | resourceOps.FindFailed<T>> {
    return this.find(operation.payload.conditions, operation.payload.relations).then(
      resources => new resourceOps.FindSuccess<T>(resources),
      error => new resourceOps.FindFailed(error.message || error)
    );
  }
}

export class DefaultFindOneBehavior<T extends IModel> extends AbstractResourceBehavior<T> implements IResourceFindOner<T> {
  findOne(conditions: Partial<T>, relations?: string[] | undefined): Promise<T> {
    return this.repo.findOne({ where: conditions, relations, cache: true }).then(
      resource => {
        if (resource) return resource;
        else throw new Error(`${this.resourceType.name} not found: ${JSON.stringify(conditions)}`);
      }
    );
  }
  execute(operation: resourceOps.FindOne<T>): Promise<resourceOps.FindOneSuccess<T> | resourceOps.FindOneFailed<T>> {
    return this.findOne(operation.payload.conditions, operation.payload.relations).then(
      resource => new resourceOps.FindOneSuccess<T>(resource),
      error => new resourceOps.FindOneFailed(error.message || error)
    );
  }
}

