import { Role } from '../../model/entity/role.model';


export interface IdentityMapItem {
  privateKey: Buffer,
  publicKey: Buffer,
  roles: Role[]
}


export interface IdentityMap {
  [subject: string]: IdentityMapItem,
  api: IdentityMapItem,
  anonymous: IdentityMapItem
}
