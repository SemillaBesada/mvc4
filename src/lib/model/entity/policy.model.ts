import { OneToOne, JoinColumn, PrimaryGeneratedColumn, Index } from 'typeorm';

import { EntityResource } from '../../database/resource-decorator';
import { CoreModelName, IModel } from '../model.interface';
import { Role } from './role.model';
import { ResourceType } from './resource-type.model';
import { Operation } from './operation.model';
import { AbstractResourceModel } from '../abstract-resource.model';


@EntityResource(CoreModelName.POLICY)
@Index('unique_entries', ['role', 'resourceType', 'operation'], { unique: true })
export class Policy implements IModel {

  @PrimaryGeneratedColumn()
  id!: number;

  @OneToOne(type => Role, user => user.id, { nullable: false })
  @JoinColumn()
  role!: Role;

  @OneToOne(type => ResourceType, rt => rt.id, { nullable: false })
  @JoinColumn()
  resourceType!: ResourceType;

  @OneToOne(type => Operation, op => op.id, { nullable: false })
  @JoinColumn()
  operation!: Operation;

}