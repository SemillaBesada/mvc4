import { PrimaryGeneratedColumn, Column, OneToOne, JoinColumn, Entity, Index, PrimaryColumn } from 'typeorm';

import { ResourceType } from './resource-type.model';
import { User } from './user.model';


@Entity()
@Index('one_user_per_resource', ['resourceId', 'type', 'user'], { unique: true })
export class Owner {

  @PrimaryColumn()
  resourceId!: number;

  @OneToOne(type => ResourceType, rt => rt.id, { primary: true })
  @JoinColumn()
  type!: ResourceType;

  @OneToOne(type => User, user => user.id, { primary: true })
  @JoinColumn()
  user!: User;
}