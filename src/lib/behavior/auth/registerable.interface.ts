import { IJwtUserInfo } from '../../operation/jwt.operation';
import { User } from '../../model/entity/user.model';


export interface IRegisterUser {
  registerUser(tokenBearer: string): Promise<User>;
}