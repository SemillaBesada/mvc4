import { IOperationBehavior } from '../operation-behavior.interface';
import { IAuthorizeable } from './authorizable.interface';
import { IPolicyAdder } from './policy-adder.interface';
import { IPolicyReader } from './policy-reader.interface';


export interface IAccessBehavior extends IOperationBehavior, IAuthorizeable, IPolicyAdder, IPolicyReader { }