import { DatabaseFactory, TestDatabaseFactory } from '../lib/database/database-factory';
import { IDatabase } from '../lib/database/database.interface';
import { IRemoteController } from '../lib/controller/remote-controller.interface';
import { IModel } from '../lib/model/model.interface';

import { OperationName } from '../lib/operation/operation.interface';
import { DefaultResourceControllerFactory } from '../lib/controller/resource-controller.factory';


describe('resource controller factory test', () => {

  const dbFactory: DatabaseFactory = new TestDatabaseFactory();
  let db: IDatabase;

  before('should create a database', async () => {
    db = await dbFactory.create();
  });

  it('should create a resource controller map', () => {

    const controllers: { [key: string]: IRemoteController } = db.coreMgr.resourceTypes
      .reduce((acc: { [key: string]: IRemoteController }, resourceType) => {

        const repo = db.getRepository<IModel>(resourceType.name);
        const controllerFactory = new DefaultResourceControllerFactory(repo, db);
        const controller = controllerFactory.create(resourceType);

        acc[resourceType.name] = controller;

        return acc;

      }, {});
  });

});