import { EntitySubscriberInterface } from "typeorm";
import { Repository } from "typeorm/repository/Repository";

import { ResourceType } from '../model/entity/resource-type.model';
import { ICoreManager } from "./core-manager.interface";


export interface IDatabase {

  coreMgr: ICoreManager;

  getRepository<T>(name: string | Function | { new(): T }): Repository<T>;

  addSubscriber(subscriber: EntitySubscriberInterface): void;
}