import { Repository } from 'typeorm/repository/Repository';
import { DeepPartial } from 'typeorm/common/DeepPartial';

import { IRemoteController } from './remote-controller.interface';
import { IOperationBehavior } from '../behavior/operation-behavior.interface';
import { IOperation, IResult, OperationName } from '../operation/operation.interface';
import { IModel } from '../model/model.interface';
import { ResourceBehaviorMap } from '../behavior/resource/resource-behavior.interface';

import { User } from '../model/entity/user.model';
import { IResourceController } from './resource-controller.interface';



export class ResourceController<T extends IModel> implements IResourceController<T> {

  constructor(private readonly behaviors: ResourceBehaviorMap<T>) { }

  get behaviorNames(): string[] {
    return Object.keys(this.behaviors);
  }

  setBehavior(name: string, behavior: IOperationBehavior & Repository<IModel>) {
    console.log('this (resource) controller does not set behaviors');
  }

  execute(operation: IOperation): Promise<IResult> {
    if (operation.type in this.behaviors)
      return this.behaviors[operation.type].execute(operation);

    else return Promise.resolve({
      type: operation.type + ':failed',
      error: 'operation not supported'
    });
  }


  create(value: DeepPartial<T>, user: User): Promise<T> {
    return this.behaviors[OperationName.CREATE].create(value, user);
  }

  read(id: number, relations?: string[] | undefined): Promise<T> {
    return this.behaviors[OperationName.READ].read(id, relations);
  }

  update(id: number, changes: Partial<T>): Promise<Partial<T>> {
    return this.behaviors[OperationName.UPDATE].update(id, changes);
  }

  remove(id: number): Promise<number> {
    return this.behaviors[OperationName.REMOVE].remove(id);
  }

  find(conditions?: Partial<T> | undefined, relations?: string[] | undefined): Promise<T[]> {
    return this.behaviors[OperationName.FIND].find(conditions, relations);
  }

  findOne(conditions: Partial<T>, relations?: string[] | undefined): Promise<T> {
    return this.behaviors[OperationName.FINDONE].findOne(conditions, relations);
  }

}