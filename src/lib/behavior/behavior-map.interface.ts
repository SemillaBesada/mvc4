import { IOperationBehavior } from './operation-behavior.interface';


export type BehaviorMap = { [behaviorName: string]: IOperationBehavior };