import { IAuthenticatable } from '../behavior/auth/authenticatable.interface';
import { IRemoteController } from './remote-controller.interface';
import { IRegisterUser } from '../behavior/auth/registerable.interface';
import { ITokenGenerator } from '../behavior/jwt/token-generator.interface';
import { User } from '../model/entity/user.model';


export interface IAuthController extends
  IRemoteController, IAuthenticatable, IRegisterUser, ITokenGenerator {

    apiUser: User;
    anonymousUser: User;
    
  }