import { Column, OneToOne, PrimaryGeneratedColumn } from 'typeorm';

import { IModel } from "./model.interface";
import { Owner } from './entity/owner.model';
import { ResourceType } from './entity/resource-type.model';


export abstract class AbstractResourceModel implements IModel {

  @PrimaryGeneratedColumn()
  id!: number;

  @OneToOne(type => ResourceType, rt => rt.id)
  resourceType!: ResourceType;
}