import { Repository } from 'typeorm';

import { IOperationBehavior } from "../operation-behavior.interface";
import { IDatabase } from '../../database/database.interface';
import { IResult, IOperation } from '../../operation/operation.interface';

import { AuthorizeOperation, AccessOperationName } from '../../operation/access.operation';
import { IApiAction } from '../../client/action.interface';
import { IAuthorizeable } from './authorizable.interface';
import { IPolicyAdder } from "./policy-adder.interface";
import { IPolicyReader } from './policy-reader.interface';
import { IAccessBehavior } from './access-behavior.interface';

import { Policy } from '../../model/entity/policy.model';
import { Owner } from '../../model/entity/owner.model';
import { User } from '../../model/entity/user.model';
import { ResourceType } from '../../model/entity/resource-type.model';
import { Operation } from '../../model/entity/operation.model';
import { Role } from '../../model/entity/role.model';

import * as accessOps from '../../operation/access.operation';


export class DefaultAccessBehavior implements IAccessBehavior {

  private readonly policyRepo: Repository<Policy>;
  private readonly ownersRepo: Repository<Owner>;
  private readonly anonymousUser = new User();

  constructor(
    private readonly db: IDatabase,
    private readonly isGodModeEnabled: boolean
  ) {
    this.anonymousUser.id = 2;
    this.anonymousUser.sub = 'anonymous';
    this.anonymousUser.roles = [];

    this.policyRepo = db.getRepository(Policy);
    this.ownersRepo = db.getRepository(Owner);
  }

  execute(operation: AuthorizeOperation): Promise<IResult> {
    return this.authorize(operation.payload.action, operation.payload.user).then(
      isAuthorized => isAuthorized === true ?
        new accessOps.AuthorizeSuccess() :
        new accessOps.AuthorizeFailed('not authorized'),
      error => new accessOps.AuthorizeFailed(error.message || error));
  }

  async authorize(action: IApiAction, aUser?: User): Promise<boolean> {
    const user = this.getUser(aUser);
    const resourceType = this.getResourceType(action);
    const operation = this.getOperation(action);

    if (this.isGod(user)) return true;

    if (Number.isInteger(action.operation.payload.id)) {

      const resourceId = action.operation.payload.id;
      const owner = await this.getOwner(resourceId, resourceType);

      if (owner && owner.user.id === user.id) {
        const permissions = await this.getPolicies(
          [this.db.coreMgr.getRole('owner')], resourceType, operation
        );

        if (permissions.length > 0) {
          return true;
        }
      }
    }

    const permissions = await this.getPolicies(user.roles, resourceType, operation);

    if (permissions.length > 0) {

      return true;
    }

    return false;
  }

  getUser(user?: User): User {
    return user && user.id && user.roles ? user : this.anonymousUser;
  }

  isGod(user: User): boolean {
    return user.id === 1 && this.isGodModeEnabled;
  }

  getResourceType(action: IApiAction): ResourceType {
    return this.db.coreMgr.getResourceType(action.model);
  }

  getOperation(action: IApiAction): Operation {
    return this.db.coreMgr.getOperation(action.operation.type);
  }

  getPolicies(roles: Role[], resourceType: ResourceType, operation: Operation): Promise<Policy[]> {
    // throw new Error('//TOEDOE: find by ids does not work on POLICY!!!')
    return this.policyRepo.findByIds(roles.map(role => ({
      role, resourceType, operation
    })), { cache: true });
  }

  getOwner(resourceId: number, resourceType: ResourceType) {
    return this.ownersRepo.findOne({
      where: { resourceId, resourceType },
      cache: true
    });
  }

  addPolicies(role: Role, resourceType: ResourceType, operations: Operation[]): Promise<Policy[]> {
    return this.policyRepo.save(this.policyRepo.create(
      operations.map(operation => ({ role, resourceType, operation }))
    ));
  }
}

