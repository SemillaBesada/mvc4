import "reflect-metadata";

import { IApiServer } from "./lib/server/server.interface";
import { DefaultApiClientFactory } from './lib/client/client.factory';
import { DefaultApiServerFactory } from './lib/server/api-server.factory';
import { TestDatabaseFactory } from './lib/database/database-factory';


export function buildServer(): Promise<IApiServer> {
  const clientFactory = new DefaultApiClientFactory();
  const serverFactory = new DefaultApiServerFactory();

  return clientFactory.create(new TestDatabaseFactory()).then(
    client => serverFactory.create(client)
  );
}

export function main() {
  return buildServer().then(
    server => server.run()
  );
}


main();