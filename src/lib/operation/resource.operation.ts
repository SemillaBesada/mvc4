import { DeepPartial } from 'typeorm/common/DeepPartial';
import { IOperation, OperationName, ResultName, FailedName, IResult, IFailed } from './operation.interface';

import { User } from '../model/entity/user.model';


export class Create<T> implements IOperation {
  readonly type: OperationName.CREATE = OperationName.CREATE;
  readonly payload: { user?: User, resource: DeepPartial<T> };
  constructor(resource: DeepPartial<T>, user?: User) {
    this.payload = { resource, user };
  }
}
export class CreateSuccess<T> implements IResult {
  readonly type: ResultName.CREATE_SUCCESS = ResultName.CREATE_SUCCESS;
  readonly payload: { resource: T };
  constructor(resource: T) {
    this.payload = { resource }
  }
}
export class CreateFailed implements IFailed {
  readonly type: FailedName.CREATE_FAILED = FailedName.CREATE_FAILED;
  constructor(public error: string) { }
}


export class Read<T> implements IOperation {
  readonly type: OperationName.READ = OperationName.READ;
  readonly payload: { id: number; relations?: string[] };
  constructor(id: number, relations?: string[]) {
    this.payload = { id, relations };
  }
}
export class ReadSuccess<T> implements IResult {
  readonly type: ResultName.READ_SUCCESS = ResultName.READ_SUCCESS;
  readonly payload: { resource: T }
  constructor(resource: T) {
    this.payload = { resource }
  }
}
export class ReadFailed<T> implements IFailed {
  readonly type: FailedName.READ_FAILED = FailedName.READ_FAILED;
  constructor(public error: string) { }
}

export class Update<T> implements IOperation {
  readonly type: OperationName.UPDATE = OperationName.UPDATE;
  readonly payload: { id: number, changes: Partial<T> };
  constructor(id: number, changes: Partial<T>) {
    this.payload = { id, changes };
  }
}
export class UpdateSuccess<T> implements IResult {
  readonly type: ResultName.UPDATE_SUCCESS = ResultName.UPDATE_SUCCESS;
  readonly payload: { resource: Partial<T> };
  constructor(resource: Partial<T>) {
    this.payload = { resource };
  }
}
export class UpdateFailed<T> implements IFailed {
  readonly type: FailedName.UPDATE_FAILED = FailedName.UPDATE_FAILED;
  constructor(public error: string) { }
}

export class Remove<T> implements IOperation {
  readonly type: OperationName.REMOVE = OperationName.REMOVE;
  readonly payload: { id: number }
  constructor(id: number) {
    this.payload = { id };
  }
}
export class RemoveSuccess<T> implements IResult {
  readonly type: ResultName.REMOVE_SUCCESS = ResultName.REMOVE_SUCCESS;
  readonly payload: { id: number }
  constructor(id: number) {
    this.payload = { id };
  }
}
export class RemoveFailed<T> implements IFailed {
  readonly type: FailedName.REMOVE_FAILED = FailedName.REMOVE_FAILED;
  constructor(public error: string) { }
}

export class Find<T> implements IOperation {
  readonly type: OperationName.FIND = OperationName.FIND;
  readonly payload: { conditions?: Partial<T>, relations?: string[] };
  constructor(conditions?: Partial<T>, relations?: string[]) {
    this.payload = { conditions, relations };
  }
}
export class FindSuccess<T> implements IResult {
  readonly type: ResultName.FIND_SUCCESS = ResultName.FIND_SUCCESS;
  readonly payload: { resources: T[] };
  constructor(resources: T[]) { this.payload = { resources } }
}
export class FindFailed<T> implements IFailed {
  readonly type: FailedName.FIND_FAILED = FailedName.FIND_FAILED;
  constructor(public error: string) { }
}

export class FindOne<T> implements IOperation {
  readonly type: OperationName.FINDONE = OperationName.FINDONE;
  readonly payload: { conditions: Partial<T>, relations?: string[] };
  constructor(conditions: Partial<T>, relations?: string[]) {
    this.payload = { conditions, relations };
  }
}
export class FindOneSuccess<T> implements IResult {
  readonly type: ResultName.FINDONE_SUCCESS = ResultName.FINDONE_SUCCESS;
  readonly payload: { resource: T };
  constructor(resource: T) { this.payload = { resource } }
}
export class FindOneFailed<T> implements IFailed {
  readonly type: FailedName.FINDONE_FAILED = FailedName.FINDONE_FAILED;
  constructor(public error: string) { }
}

