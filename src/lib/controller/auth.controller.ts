import { IAuthController } from "./auth-controller.interface";
import { IOperationBehavior } from '../behavior/operation-behavior.interface';
import { BehaviorMap } from '../behavior/behavior-map.interface';
import { AuthOperationName } from '../operation/auth.operation';

import * as authOps from '../operation/auth.operation';

import { IAuthenticatable } from '../behavior/auth/authenticatable.interface';
import { IOperation, IResult } from '../operation/operation.interface';
import { User } from '../model/entity/user.model';
import { IRegisterUser } from '../behavior/auth/registerable.interface';
import { ITokenGenerator } from "../behavior/jwt/token-generator.interface";


export class AuthController implements IAuthController {

  private readonly behaviors: BehaviorMap = {};
  private readonly authenticator: IAuthenticatable;
  private readonly userRegistrator: IRegisterUser;

  constructor(
    public readonly apiUser: User,
    public readonly anonymousUser: User,
    private readonly tokenGenerator: ITokenGenerator,
    registerBehavior: IOperationBehavior & IRegisterUser,
    loginBehavior: IOperationBehavior & IAuthenticatable,
    logoutBehavior: IOperationBehavior
  ) {
    this.behaviors[AuthOperationName.REGISTER] = registerBehavior;
    this.behaviors[AuthOperationName.LOGIN] = loginBehavior;
    this.behaviors[AuthOperationName.LOGOUT] = logoutBehavior;

    this.authenticator = loginBehavior;
    this.userRegistrator = registerBehavior;
  }

  get behaviorNames(): string[] {
    return Object.keys(this.behaviors);
  }

  setBehavior(name: string, behavior: IOperationBehavior): void {
    console.log('this (auth) controller does not set behaviors');
  }

  execute(operation: IOperation): Promise<IResult> {

    if (operation.type in this.behaviors) {
      return this.behaviors[operation.type].execute(operation);
    }

    else return Promise.resolve({
      type: operation.type + ':failed',
      error: 'operation not supported'
    });
  }

  generateToken(subject: string): Promise<string> {
    return this.tokenGenerator.generateToken(subject);
  }

  authenticate(tokenBearer: string, subject?: string): Promise<User> {
    return this.authenticator.authenticate(tokenBearer, subject);
  }

  registerUser(tokenBearer: string): Promise<User> {
    return this.userRegistrator.registerUser(tokenBearer);
  }
}