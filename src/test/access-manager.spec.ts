import { expect } from "chai";

import { TestDatabaseFactory } from '../lib/database/database-factory';
import { AccessController } from '../lib/controller/access-controller';
import { IApiClient } from '../lib/client/client.interface';
import { DefaultApiClientFactory } from '../lib/client/client.factory';

import * as resourceOps from '../lib/operation/resource.operation';
import * as accessOps from '../lib/operation/access.operation';

import { Policy } from "../lib/model/entity/policy.model";
import { User } from '../lib/model/entity/user.model';


describe('AccessManager test', () => {

  let client: IApiClient;


  before('it should create an AccessManager', async () => {
    const clientFactory = new DefaultApiClientFactory();

    client = await clientFactory.create(new TestDatabaseFactory());

    console.log(client.coreInfo);
  });


  it('should authorize all actions as failed', async () => {

    const user = new User();
    user.id = 1;

    const result = await client.execute({
      model: 'access',
      operation: new accessOps.AuthorizeOperation({
        model: 'policy',
        operation: new resourceOps.Create<Policy>({})
      }, user)
    });

    console.log(result);

  });


});