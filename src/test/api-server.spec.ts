import { TestDatabaseFactory } from '../lib/database/database-factory';
import { ApiClientFactory, DefaultApiClientFactory } from '../lib/client/client.factory';
import { IApiClient } from '../lib/client/client.interface';
import { DefaultApiServerFactory } from '../lib/server/api-server.factory';
import { IApiServer } from '../lib/server/server.interface';


describe('APi Server test', () => {

  let client: IApiClient;
  let server: IApiServer;

  before('it should create a api server', async () => {

    const clientFactory = new DefaultApiClientFactory();
    const serverFactory = new DefaultApiServerFactory();

    client = await clientFactory.create(new TestDatabaseFactory());
    server = serverFactory.create(client);

  });

  it('should run the server', (done) => {
    server.run();
    setTimeout(() => {
      console.log('\njust waiting a bit...(500ms)');
      done();
    }, 500);
  });

  it('should do kakpipi the server', () => {

    console.log('kak pipi?')

  });

});