import { Repository } from 'typeorm';

import { IAccessController } from './access-controller.interface';
import { BehaviorMap } from '../behavior/behavior-map.interface';
import { IOperationBehavior } from '../behavior/operation-behavior.interface';
import { IAuthorizeable } from '../behavior/access/authorizable.interface';
import { IPolicyAdder } from '../behavior/access/policy-adder.interface';
import { IPolicyReader } from '../behavior/access/policy-reader.interface';
import { IAccessBehavior } from '../behavior/access/access-behavior.interface';

import { AccessOperationName } from '../operation/access.operation';
import { IOperation, IResult } from '../operation/operation.interface';
import { IApiAction } from '../client/action.interface';

import { User } from '../model/entity/user.model';
import { Role } from '../model/entity/role.model';
import { ResourceType } from '../model/entity/resource-type.model';
import { Operation } from '../model/entity/operation.model';
import { Policy } from '../model/entity/policy.model';






export class AccessController implements IAccessController {

  private readonly behaviors: BehaviorMap = {};
  
  private readonly authorizer: IAuthorizeable;
  private readonly policyAdder: IPolicyAdder;
  private readonly policyReader: IPolicyReader;

  constructor(accessBehavior: IAccessBehavior) {
    this.behaviors[AccessOperationName.AUTHORIZE] = accessBehavior;
    this.authorizer = accessBehavior;
    this.policyAdder = accessBehavior;
    this.policyReader = accessBehavior;
  }

  get behaviorNames(): string[] {
    return Object.keys(this.behaviors);
  }

  setBehavior(name: string, behavior: IOperationBehavior): void {
    console.log('this (access) controller does not set behaviors');
  }

  execute(operation: IOperation): Promise<IResult> {
    if (operation.type in this.behaviors)
      return this.behaviors[operation.type].execute(operation);

    else return Promise.resolve({
      type: operation.type + ':failed',
      error: 'operation not supported'
    });
  }

  authorize(action: IApiAction, user?: User): Promise<boolean> {
    return this.authorizer.authorize(action, user);
  }

  getPolicies(roles: Role[], resourceType: ResourceType, operation: Operation): Promise<Policy[]> {
    return this.policyReader.getPolicies(roles, resourceType, operation);
  }

  addPolicies(role: Role, resourceType: ResourceType, operations: Operation[]): Promise<Policy[]> {
    return this.policyAdder.addPolicies(role, resourceType, operations);
  }

}