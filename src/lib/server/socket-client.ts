import { IApiSocketClient } from './socket-client.interface';
import { IApiClient } from '../client/client.interface';

import * as authOps from '../operation/auth.operation';
import * as crudOps from '../operation/resource.operation';

import { User } from '../model/entity/user.model';


export class ApiSocketClient implements IApiSocketClient {

  getSocket(socketId: string): SocketIO.Socket {
    return this.socketServer.sockets.sockets[socketId];
  }

  getUser(socket: SocketIO.Socket): User {
    return socket.handshake.query.user;
  }

  constructor(
    public readonly apiClient: IApiClient,
    public readonly socketServer: SocketIO.Server
  ) {
    socketServer.use((socket, next) => {

      const tokenBearer = socket.handshake.query.accessToken;
      const isAnonymous = +socket.handshake.query.anonymous > 0;
      const sub = isAnonymous ? 'anonymous' : undefined;

      apiClient.authenticate(tokenBearer, sub).then(
        user => {

          console.log('matching user:', user);

          socket.handshake.query.user = user;
          next();
        },
        error => {
          this.logMessage(`error: ${error.message}, returning anonymous`);
          socket.handshake.query.anonymous = 1;
          socket.handshake.query.user = this.apiClient.anonymousUser;
          next();
        }
      );
    });

    socketServer.on('connection', (socket: SocketIO.Socket) =>
      this.onConnect(socket)
    );
  }

  onLogin(socketId: string, tokenBearer: string): void {
    const socket = this.getSocket(socketId);


    this.apiClient.authenticate(tokenBearer).then(
      user => {
        // this.sessions.add(socketId);
        //socket.emit('auth', new authOps.LoginSuccess(user));
        socket.send({ type: 'auth:signin:success', payload: { user } });
      },
      error => {
        // socket.emit('auth', new authOps.LoginFailed(error.message || error));
        socket.send({ type: 'auth:signin:failed', payload: { error: error.message } });
      }
    );


  }

  onRegister(socketId: string, tokenBearer: string): void {

    const socket = this.getSocket(socketId);

    this.apiClient.registerUser(tokenBearer).then(
      //user => socket.emit('auth', new authOps.RegisterSuccess(user)),
      user => {

        socket.send({ type: 'auth:signup:success', payload: { user } });
      },
      error => socket.send({ type: 'auth:signup:failed', payload: { error: error.message } })
    );

  }

  onLogout(socketId: string, subject: string): void {
    console.log('user logging out...', subject)
    const socket = this.getSocket(socketId);

    socket.send({ type: 'auth:signout:success' });
  }

  onMessage(socketId: string, message: any): void {
    const socket = this.getSocket(socketId);
    const user = this.getUser(socket);
    
    this.logMessage(JSON.stringify(message));
    this.apiClient.runCommand(message, user).then(
      result => socket.send(result)
    );
  }

  onConnect(socket: SocketIO.Socket) {

    const user = this.getUser(socket);

    socket.on('login', ({ token, sub }) => this.onLogin(socket.id, token));
    socket.on('signup', ({ token }) => this.onRegister(socket.id, token));
    socket.on('logout', (subject) => this.onLogout(socket.id, subject));
    socket.on('message', message => this.onMessage(socket.id, message));

    socket.send(new authOps.LoginSuccess(user));
    //if (user.sub !== 'anonymous')
    // socket.send({ type: 'auth:signin:success', payload: { user } });
  }

  onDisconnect(reason: any): void {

    this.logMessage('disconnected ' + reason);
    // message
  }

  onError(error: any) {
    this.logMessage(error);
  }

  logMessage(text: string) {
    console.log(`[SocketClient]: ${text}`);
  }
}