import { IDatabase } from '../database/database.interface';
import { IApiClient } from './client.interface';
import { IRemoteController } from '../controller/remote-controller.interface';
import { IModel, CoreModelName } from '../model/model.interface';
import { IResourceController } from '../controller/resource-controller.interface';
import { IdentityMap } from '../behavior/jwt/identity-map.interface';
import { IClientIdentityMapFactory, DefaultClientIdentityMapFactory } from './client-identity-map.factory';

import { DatabaseFactory } from '../database/database-factory';
import { DefaultResourceControllerFactory } from '../controller/resource-controller.factory';
import { DefaultAuthControllerFactory } from '../controller/default-auth-controller.factory';

import { ApiClient } from './client';
import { AccessController } from '../controller/access-controller';
import { OperationName } from '../operation/operation.interface';

import { User } from '../model/entity/user.model';
import { ResourceType } from '../model/entity/resource-type.model';
import { Policy } from '../model/entity/policy.model';

import * as accessBehaviors from '../behavior/access/access.behavior';


export abstract class ApiClientFactory {

  create(dbFactory: DatabaseFactory, clientIdentityMapFactory: IClientIdentityMapFactory = new DefaultClientIdentityMapFactory()): Promise<IApiClient> {
    return this.createClient(dbFactory, clientIdentityMapFactory.createIdentityMap());
  }

  protected abstract createClient(dbFactory: DatabaseFactory, identities: IdentityMap): Promise<IApiClient>;
}


export class DefaultApiClientFactory extends ApiClientFactory {

  async createClient(dbFactory: DatabaseFactory, identities: IdentityMap): Promise<IApiClient> {

    const db = await dbFactory.create();
    const controllers: { [key: string]: IRemoteController } = {};
    const userController = this.createResourceController<User>(
      db, db.coreMgr.getResourceType(CoreModelName.USER)
    );

    db.coreMgr.resourceTypes
      .filter(type => type.name !== CoreModelName.USER)
      .forEach(resourceType => {
        controllers[resourceType.name] = this.createResourceController(db, resourceType);
      });

    const authControllerFact = new DefaultAuthControllerFactory(
      db.coreMgr.apiUser,
      db.coreMgr.anonymousUser,
      identities,
      userController
    );

    const authController = await authControllerFact.create();
    const accessController = new AccessController(
      new accessBehaviors.DefaultAccessBehavior(db, dbFactory.isTestDb)
    );

    // controllers.auth = authController;
    // controllers.access = accessController;
    controllers[CoreModelName.USER] = userController;

    return new ApiClient(
      controllers,
      authController,
      accessController
    );
  }

  createResourceController<T extends IModel>(db: IDatabase, resourceType: ResourceType): IResourceController<T> {

    const controllerFact = new DefaultResourceControllerFactory<T>(db.getRepository(resourceType.name), db);
    const resourceController = controllerFact.create(resourceType);

    return resourceController;
  }


}