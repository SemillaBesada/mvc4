import { IOperation, ResultName, IResult, FailedName, IFailed } from './operation.interface';

import { User } from '../model/entity/user.model';
import { IApiAction } from '../client/action.interface';

export enum AccessOperationName {
  AUTHORIZE = 'authorize'
}

export enum AccessResultName {
  AUTHORIZE_SUCCESS = 'authorize:success',
}

export enum AccessFailedName {
  AUTHORIZE_FAILED = 'authorize:failed',
}


export class AuthorizeOperation implements IOperation {
  readonly type: AccessOperationName.AUTHORIZE = AccessOperationName.AUTHORIZE;
  readonly payload: { action: IApiAction, user?: User };
  constructor(action: IApiAction, user?: User) {
    this.payload = { action, user };
  }
}

export class AuthorizeSuccess implements IResult {
  readonly type: AccessResultName.AUTHORIZE_SUCCESS = AccessResultName.AUTHORIZE_SUCCESS;
}

export class AuthorizeFailed implements IFailed {
  readonly type: AccessFailedName.AUTHORIZE_FAILED = AccessFailedName.AUTHORIZE_FAILED;
  constructor(public error: string) { }
}