import { IRemoteController } from './remote-controller.interface';
import { ResourceType } from '../model/entity/resource-type.model';


export abstract class AbstractControllerFactory {
  abstract create(resourceType: ResourceType): IRemoteController;
}