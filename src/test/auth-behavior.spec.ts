import * as fs from 'fs';
import { expect } from 'chai';

import { DatabaseFactory, TestDatabaseFactory } from '../lib/database/database-factory';
import { IDatabase } from '../lib/database/database.interface';
import { IRemoteController } from '../lib/controller/remote-controller.interface';
import { IModel, CoreModelName } from '../lib/model/model.interface';
import { IOperationBehavior } from '../lib/behavior/operation-behavior.interface';
import { ITokenGenerator } from '../lib/behavior/jwt/token-generator.interface';

import { OperationName } from '../lib/operation/operation.interface';
import { AuthResultName } from '../lib/operation/auth.operation';
import { User } from '../lib/model/entity/user.model';
import { Role } from '../lib/model/entity/role.model';

import { DefaultResourceControllerFactory } from '../lib/controller/resource-controller.factory';
import { DefaultAuthControllerFactory } from '../lib/controller/default-auth-controller.factory';

import * as authOps from '../lib/operation/auth.operation';
import * as jwtOps from '../lib/operation/jwt.operation';
import * as jwtBehaviors from '../lib/behavior/jwt/jwt.behavior';


describe('auth controller test', () => {

  const dbFactory: DatabaseFactory = new TestDatabaseFactory();

  let db: IDatabase;
  let authController: IRemoteController;

  let apiTokenBearer: string;
  let anonymousTokenBearer: string;

  let tokenGenerator: IOperationBehavior & ITokenGenerator;


  before('it should initialize the auth controller', async () => {

    db = await dbFactory.create();

    const userRepo = db.getRepository(User);
    const userCtrlFact = new DefaultResourceControllerFactory(userRepo, db);
    const userController = userCtrlFact.create(db.coreMgr.getResourceType(CoreModelName.USER));
    const adminRole = db.coreMgr.getRole('admin');

    const privateApiKey = fs.readFileSync(`${__dirname}/../../ssl/api_private_key.pem`);
    const privateAnonymousKey = fs.readFileSync(`${__dirname}/../../ssl/anonymous_private_key.pem`);
    const publicApiKey = fs.readFileSync(`${__dirname}/../../ssl/api_public_key.pem`);
    const publicAnonymousKey = fs.readFileSync(`${__dirname}/../../ssl/anonymous_public_key.pem`);

    const identities = {
      api: {
        privateKey: privateApiKey,
        publicKey: publicApiKey,
        roles: [adminRole]
      },
      anonymous: {
        privateKey: privateAnonymousKey,
        publicKey: publicAnonymousKey,
        roles: []
      }
    };

    tokenGenerator = new jwtBehaviors.DefaultGenerateJwtBehavior(identities);

    const authCtrlFact = new DefaultAuthControllerFactory(
      db.coreMgr.apiUser, db.coreMgr.anonymousUser, identities, userController
    );

    authController = await authCtrlFact.create();
    apiTokenBearer = await tokenGenerator.generateToken('api');
    anonymousTokenBearer = await tokenGenerator.generateToken('anonymous');

    expect(apiTokenBearer).a('string');
    expect(anonymousTokenBearer).a('string');
  });


  it('should let api login', async () => {

    const result = await authController.execute(
      new authOps.Login(apiTokenBearer, 'api')
    );

    if (result.error) throw new Error(result.error);

    const user: User = result.payload.user;

    expect(result.type).eql(AuthResultName.LOGIN_SUCCESS);
    expect(user.id).eql(1);
    expect(user.sub).eql('api');

    console.log('api user', user);
  });

  it('should let anonyomus login', async () => {

    const result = await authController.execute(
      new authOps.Login(anonymousTokenBearer, 'anonymous')
    );

    if (result.error) throw new Error(result.error);

    const user: User = result.payload.user;

    expect(result.type).eql(AuthResultName.LOGIN_SUCCESS);
    expect(user.id).eql(2);
    expect(user.sub).eql('anonymous');

    console.log('anonymous user', user);
  });

  it('should NOT let anonyomus login with admin key', async () => {

    const result = await authController.execute(
      new authOps.Login(apiTokenBearer, 'anonymous')
    );

    expect(result.type).not.eql(AuthResultName.LOGIN_SUCCESS);
  });

  it('should NOT let anonyomus login with invalid key', async () => {

    const result = await authController.execute(
      new authOps.Login(apiTokenBearer + 'pipikaka', 'anonymous')
    );

    expect(result.type).not.eql(AuthResultName.LOGIN_SUCCESS);
  });
});