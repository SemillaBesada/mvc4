import * as http from 'http';
import * as SocketIO from 'socket.io';
import * as express from 'express'

import { IApiServer } from './server.interface';
import { ApiServer } from './server';
import { IApiClient } from '../client/client.interface';


export abstract class AbstractApiServerFactory {

  create(client: IApiClient): IApiServer {

    const app = this.createExpressApp();
    const http = this.createWebServer(app);
    const socket = this.createWebSocket(http);

    return new ApiServer(client, app, http, socket);
  }

  abstract createExpressApp(): express.Application;
  abstract createWebServer(app: express.Application): http.Server;
  abstract createWebSocket(http: http.Server): SocketIO.Server;
}


export class DefaultApiServerFactory extends AbstractApiServerFactory {

  createExpressApp(): express.Application {
    const app = express();

    return app;
  }

  createWebServer(app: express.Application): http.Server {
    return http.createServer(app);
  }

  createWebSocket(webServer: http.Server): SocketIO.Server {
    return SocketIO(webServer, { cookie: false });
  }
}