import { expect } from "chai";

import { DatabaseFactory, TestDatabaseFactory } from "../lib/database/database-factory";
import { IDatabase } from "../lib/database/database.interface";
import { OperationName, ResultName, IResult, IFailed } from '../lib/operation/operation.interface';
import { CoreHelper } from '../lib/database/resource-decorator';


describe('Core service test', () => {

  const dbFactory: DatabaseFactory = new TestDatabaseFactory();  
  let db: IDatabase;

  before('should create a database', async () => {
    db = await dbFactory.create();    
  });

  it('should list all registered resource types',() => {
    console.log('registerd names', CoreHelper.registeredTypes);

    expect(Object.keys(CoreHelper.registeredTypes).length).greaterThan(1);
  })

});