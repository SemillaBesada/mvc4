import { IOperation, ResultName, IResult, FailedName, IFailed } from './operation.interface';


export enum JwtOperationName {
  JWT_GENERATE = 'jwtGenerate',
  JWT_VERIFY = 'jwtVerify',
}

export enum JwtOperationResultName {
  JWT_GENERATE_SUCCESS = 'jwtGenerate:success',
  JWT_VERIFY_SUCCESS = 'jwtVerify:success',
}

export enum JwtOperationFailedName {
  JWT_GENERATE_FAILED = 'jwtGenerate:failed',
  JWT_VERIFY_FAILED = 'jwtVerify:failed',
}

export interface IJwtUserInfo {
  sub: string;
  name: string;
  email: string;
  email_verified: boolean;
  nickname?: string;
  picture?: string;
}


export class GenerateJwtOperation implements IOperation {
  readonly type: JwtOperationName.JWT_GENERATE = JwtOperationName.JWT_GENERATE;
  readonly payload: { sub: string }
  constructor(sub: string) {
    this.payload = { sub };
  }
}

export class GenerateJwtSuccess implements IResult {
  readonly type: JwtOperationResultName.JWT_GENERATE_SUCCESS = JwtOperationResultName.JWT_GENERATE_SUCCESS;
  readonly payload: { tokenBearer: string }
  constructor(tokenBearer: string) {
    this.payload = { tokenBearer };
  }
}

export class GenerateJwtFailed implements IFailed {
  readonly type: JwtOperationFailedName.JWT_GENERATE_FAILED = JwtOperationFailedName.JWT_GENERATE_FAILED;
  constructor(public readonly error: string) { }
}


export class VerifyJwtOperation implements IOperation {
  readonly type: JwtOperationName.JWT_VERIFY = JwtOperationName.JWT_VERIFY;
  readonly payload: { tokenBearer: string, subject?: string, };
  constructor(tokenBearer: string, subject?: string) {
    this.payload = { tokenBearer, subject };
  }
}

export class VerifyJwtSuccess implements IResult {
  readonly type: JwtOperationResultName.JWT_VERIFY_SUCCESS = JwtOperationResultName.JWT_VERIFY_SUCCESS;
  readonly payload: { userInfo: IJwtUserInfo }
  constructor(userInfo: IJwtUserInfo) {
    this.payload = { userInfo };
  }
}

export class VerifyJwtFailed implements IFailed {
  readonly type: JwtOperationFailedName.JWT_VERIFY_FAILED = JwtOperationFailedName.JWT_VERIFY_FAILED;
  constructor(public readonly error: string) { }
}