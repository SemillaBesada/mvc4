import { IOperation, ResultName, IResult, FailedName, IFailed } from './operation.interface';

import { User } from '../model/entity/user.model';


export enum AuthOperationName {
  REGISTER = 'register',
  LOGIN = 'login',
  LOGOUT = 'logout',
}

export enum AuthResultName {
  REGISTER_SUCCESS = 'register:success',
  LOGIN_SUCCESS = 'login:success',
  LOGOUT_SUCCESS = 'logout:success',
}

export enum AuthFailedName {
  REGISTER_FAILED = 'register:failed',
  LOGIN_FAILED = 'login:failed',
  LOGOUT_FAILED = 'logout:failed',
}


export class Register implements IOperation {
  readonly type: AuthOperationName.REGISTER = AuthOperationName.REGISTER;
  constructor(public payload: { tokenBearer: string }) { }
}
export class RegisterSuccess implements IResult {
  readonly type: AuthResultName.REGISTER_SUCCESS = AuthResultName.REGISTER_SUCCESS;
  readonly payload: { user: User };
  constructor(user: User) { this.payload = { user } }
}
export class RegisterFailed implements IFailed {
  readonly type: AuthFailedName.REGISTER_FAILED = AuthFailedName.REGISTER_FAILED;
  constructor(public error: string) { }
}

export class Login implements IOperation {
  readonly type: AuthOperationName.LOGIN = AuthOperationName.LOGIN;
  readonly payload: { tokenBearer: string, subject?: string }
  constructor(tokenBearer: string, subject?: string) {
    this.payload = { tokenBearer, subject };
  }
}
export class LoginSuccess implements IResult {
  readonly type: AuthResultName.LOGIN_SUCCESS = AuthResultName.LOGIN_SUCCESS;
  readonly payload: { user: User };
  constructor(user: User) { this.payload = { user } }
}
export class LoginFailed implements IFailed {
  readonly type: AuthFailedName.LOGIN_FAILED = AuthFailedName.LOGIN_FAILED;
  constructor(public error: string) { }
}

export class Logout implements IOperation {
  readonly type: AuthOperationName.LOGOUT = AuthOperationName.LOGOUT;
  readonly payload: { user: User };
  constructor(user: User) { this.payload = { user } }
}
export class LogoutSuccess implements IResult {
  readonly type: AuthResultName.LOGOUT_SUCCESS = AuthResultName.LOGOUT_SUCCESS;
}
export class LogoutFailed implements IFailed {
  readonly type: AuthFailedName.LOGOUT_FAILED = AuthFailedName.LOGOUT_FAILED;
  constructor(public error: string) { }
}

