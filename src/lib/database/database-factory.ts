import "reflect-metadata";
import { createConnection, Connection } from "typeorm";

import { IDatabase } from './database.interface';
import { Database } from './database';

import { ResourceType } from '../model/entity/resource-type.model';
import { CoreModelName } from '../model/model.interface';
import { Operation } from '../model/entity/operation.model';
import { CoreManager } from './core-manager';
import { ICoreManager } from './core-manager.interface';

import { User } from '../model/entity/user.model';
import { Owner } from "../model/entity/owner.model";
import { Role } from "../model/entity/role.model";
import { CoreHelper } from './resource-decorator';


export abstract class DatabaseFactory {

  public abstract isTestDb: boolean;
  public readonly coreMgr: ICoreManager;


  constructor(protected resourceDirs: string[] = []) {
    this.coreMgr = new CoreManager(CoreHelper.registeredTypes);
  }

  addResourceDirectory(path: string): void {
    this.resourceDirs.push(path);
  }

  create(): Promise<IDatabase> {
    return this.connect().then(
      async connection => {

        const resourceTypeRepo = connection.getRepository(ResourceType);

        if (connection.options.dropSchema === true) {

          const typesToCreate = resourceTypeRepo.create(this.coreMgr.resourceTypes);
          const resourceTypes = await resourceTypeRepo.save(typesToCreate);
          const userRepo = connection.getRepository(User);
          const userResourceType = this.coreMgr.getResourceType(CoreModelName.USER);

          await connection.getRepository(Operation).insert(
            this.coreMgr.operations
          );

          await connection.getRepository(Role).insert(
            this.coreMgr.roles
          );

          const apiUser = await userRepo.save(this.coreMgr.apiUser);
          const anonymousUser = await userRepo.save(this.coreMgr.anonymousUser);

          await connection.getRepository(Owner).insert([
            { user: apiUser, resourceId: 1, type: userResourceType },
            { user: apiUser, resourceId: 2, type: userResourceType }
          ]);

          return new Database(connection, this.coreMgr);

        } else {

          return new Database(connection, this.coreMgr);

        }
      }
    );
  }

  protected abstract connect(): Promise<Connection>;
}


export class TestDatabaseFactory extends DatabaseFactory {
  public readonly isTestDb: true = true;
  connect(): Promise<Connection> {
    return createConnection({
      type: 'mysql',
      entities: [
        `${__dirname}/../model/entity/**/*.model.[jt]s`,
        ...this.resourceDirs
      ],
      username: 'root',
      password: 'toor',
      database: 'test',
      dropSchema: true,
      synchronize: true,
    }).then(connection => connection);
  }
}