import * as fs from 'fs';
import { expect } from 'chai';

import { IOperationBehavior } from '../lib/behavior/operation-behavior.interface';
import { ITokenGenerator } from '../lib/behavior/jwt/token-generator.interface';
import { ITokenDecoder } from '../lib/behavior/jwt/token-decoder.interface';

import * as jwtBehaviors from '../lib/behavior/jwt/jwt.behavior';
import * as jwtOps from '../lib/operation/jwt.operation';

import { Role } from '../lib/model/entity/role.model';
import { JwtOperationResultName } from '../lib/operation/jwt.operation';


describe('Jwt behavior test', () => {

  const adminRole = new Role();
  adminRole.id = 1;
  adminRole.name = 'admin';

  let privateApiKey: Buffer;
  let privateAnonymousKey: Buffer;
  let publicApiKey: Buffer;
  let publicAnonymousKey: Buffer;

  let generateJwtBehavior: IOperationBehavior & ITokenGenerator;
  let verifyJwtBehavior: IOperationBehavior & ITokenDecoder;

  let apiBearer: string;
  let anonymousBearer: string;


  before('it should read the key files', () => {

    privateApiKey = fs.readFileSync(`${__dirname}/../../ssl/api_private_key.pem`);
    privateAnonymousKey = fs.readFileSync(`${__dirname}/../../ssl/anonymous_private_key.pem`);

    publicApiKey = fs.readFileSync(`${__dirname}/../../ssl/api_public_key.pem`);
    publicAnonymousKey = fs.readFileSync(`${__dirname}/../../ssl/anonymous_public_key.pem`);

    const identities = {
      api: {
        privateKey: privateApiKey,
        publicKey: publicApiKey,
        roles: [adminRole]
      },
      anonymous: {
        privateKey: privateAnonymousKey,
        publicKey: publicAnonymousKey,
        roles: []
      }
    };

    generateJwtBehavior = new jwtBehaviors.DefaultGenerateJwtBehavior(identities);
    verifyJwtBehavior = new jwtBehaviors.DefaultVerifyJwtBehavior(identities);
  });


  it('should generate a jwt for admin', async () => {
    const op = await generateJwtBehavior.execute(
      new jwtOps.GenerateJwtOperation('api')
    );

    expect(op.type).eql(JwtOperationResultName.JWT_GENERATE_SUCCESS);

    apiBearer = op.payload.tokenBearer;
  });

  it('should generate a token for anonymous', async () => {
    const op = await generateJwtBehavior.execute(
      new jwtOps.GenerateJwtOperation('anonymous')
    );

    expect(op.type).eql(JwtOperationResultName.JWT_GENERATE_SUCCESS);

    anonymousBearer = op.payload.tokenBearer;
  });

  it('should verify the generated admin token', async () => {

    const op = await verifyJwtBehavior.execute(
      new jwtOps.VerifyJwtOperation(anonymousBearer, 'anonymous')
    );

    expect(op.type).eql(JwtOperationResultName.JWT_VERIFY_SUCCESS);

    if (op.error) throw new Error(op.error);
  });

  it('should verify the generated anonymous token', async () => {

    const op = await verifyJwtBehavior.execute(
      new jwtOps.VerifyJwtOperation(apiBearer, 'api')
    );

    expect(op.type).eql(JwtOperationResultName.JWT_VERIFY_SUCCESS);

    if (op.error) throw new Error(op.error);
  });

});