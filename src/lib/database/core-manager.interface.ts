import { CoreManager } from './core-manager';

import { ResourceType } from '../model/entity/resource-type.model';
import { Operation } from '../model/entity/operation.model';
import { Role } from '../model/entity/role.model';
import { User } from '../model/entity/user.model';


export interface ICoreManager {

  resourceTypes: ResourceType[];
  operations: Operation[];
  roles: Role[];

  getResourceType(name: string): ResourceType;
  getOperation(name: string): Operation;
  getRole(name: string): Role;

  apiUser: User;
  anonymousUser: User;
}

