import { IOperation, IResult, IFailed } from '../operation/operation.interface';


export interface IApiAction {
  model: string;
  operation: IOperation;
}

export interface IApiResult {
  model: string;
  result: IResult;
  error?: string;
}

