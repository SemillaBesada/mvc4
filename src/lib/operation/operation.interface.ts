import { IModel } from "../model/model.interface";

export enum OperationName {
  CREATE = 'create',
  READ = 'read',
  UPDATE = 'update',
  REMOVE = 'remove',
  FIND = 'find',
  FINDONE = 'findOne',
}

export enum ResultName {
  CREATE_SUCCESS = 'create:success',
  READ_SUCCESS = 'read:success',
  UPDATE_SUCCESS = 'update:success',
  REMOVE_SUCCESS = 'remove:success',
  FIND_SUCCESS = 'find:success',
  FINDONE_SUCCESS = 'findOne:success',
}

export enum FailedName {
  CREATE_FAILED = 'create:failed',
  READ_FAILED = 'read:failed',
  UPDATE_FAILED = 'update:failed',
  REMOVE_FAILED = 'remove:failed',
  FIND_FAILED = 'find:failed',
  FINDONE_FAILED = 'findOne:failed',
}

export interface IOperation {
  readonly type: string;
  payload?: any;
}

export interface IResult extends IOperation {
  error?: string;
}

export interface IFailed extends IResult {
  readonly error: string;
}
