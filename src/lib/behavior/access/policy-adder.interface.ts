import { Role } from '../../model/entity/role.model';
import { ResourceType } from '../../model/entity/resource-type.model';
import { Operation } from '../../model/entity/operation.model';
import { Policy } from '../../model/entity/policy.model';


export interface IPolicyAdder {
  addPolicies(role: Role, resourceType: ResourceType, operations: Operation[]): Promise<Policy[]>;
}