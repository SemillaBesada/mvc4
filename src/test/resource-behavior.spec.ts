import { expect } from "chai";

import { CoreHelper } from '../lib/database/resource-decorator';
import { DatabaseFactory, TestDatabaseFactory } from "../lib/database/database-factory";
import { IDatabase } from "../lib/database/database.interface";
import { OperationName, ResultName, IResult, IFailed } from '../lib/operation/operation.interface';
import { CoreModelName } from '../lib/model/model.interface';

import { DefaultResourceControllerFactory } from '../lib/controller/resource-controller.factory';
import { IResourceController } from '../lib/controller/resource-controller.interface';

import { User } from '../lib/model/entity/user.model';

import * as behaviors from '../lib/behavior/resource/resource-behavior';
import * as operations from '../lib/operation/resource.operation';



describe('Default behaviors test', () => {

  let userController: IResourceController<User>;

  let db: IDatabase;
  let testUser: User;


  before('should create a database and a ResourceController<User>', async () => {
    const dbFactory: DatabaseFactory = new TestDatabaseFactory();
    db = await dbFactory.create();

    const ctrlFactory = new DefaultResourceControllerFactory(db.getRepository(User), db);
    userController = ctrlFactory.create(db.coreMgr.getResourceType(CoreModelName.USER));
  });


  it('should contain all resources decorated with @EntityResource()', () => {
    const resourceMap = CoreHelper.registeredTypes;
    const allResources = Object.keys(resourceMap).map(
      key => resourceMap[key].name
    );

    db.coreMgr.resourceTypes.forEach(type => {
      expect(allResources.includes(type.name))
    });
  });

  it('should create user', async () => {
    const op = new operations.Create<User>({
      sub: 'remi|98',
      email: 'RemiRules@semilla-besada.com'
    });
    const result = await userController.execute(op);

    if (result.type === ResultName.CREATE_SUCCESS) {
      expect(result.payload.resource.id).a('number');
      expect(result.payload.resource.email).eql(op.payload.resource.email);
    }
    else throw new Error(result.error);
  });

  it('should read user', async () => {
    const op = new operations.Read(1);
    const result = await userController.execute(op);

    if (result.type === ResultName.READ_SUCCESS) {
      testUser = result.payload.resource;
      expect(testUser.id).eql(op.payload.id);
    }
    else throw new Error('update failed' + result.type);
  });

  it('should update user', async () => {
    const op = new operations.Update(
      testUser.id, { email: 'dikke tigro' }
    );
    const result = await userController.execute(op);

    if (result.type === ResultName.UPDATE_SUCCESS) {
      const update: Partial<User> = result.payload.resource;
      expect(update.email).eql(op.payload.changes.email);
    }
    else throw new Error('update failed' + result);
  });

  it('should add a couple of users and find them all', () => {
    return Promise.all([
      userController.execute(new operations.Create({ sub: 'user1', email: 'email1' })),
      userController.execute(new operations.Create({ sub: 'user2', email: 'email2' })),
      userController.execute(new operations.Create({ sub: 'user3', email: 'email3' }))
    ]);
  });

  it('should find 6 users (+admin and anonymous)', async () => {
    const result = await userController.execute(new operations.Find());

    if (result.type === ResultName.FIND_SUCCESS) {
      const users: User[] = result.payload.resources;
      expect(users.length).gte(4);
    }
    else throw new Error(result.error);
  });

  it('should find user3', async () => {
    const result = await userController.execute(new operations.FindOne<User>({ sub: 'user3' }));

    expect(result.type).eql(ResultName.FINDONE_SUCCESS);
    const user: User = result.payload.resource;
    expect(user.sub).eql('user3');
  });

  it('should remove user3', async () => {
    const result = await userController.execute(new operations.Remove(4));
    const allUsersResult = await userController.execute(new operations.Find());
    const users = allUsersResult.payload.resources;

    expect(result.type).eql(ResultName.REMOVE_SUCCESS);
  });

});