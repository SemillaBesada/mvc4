import { IRemoteController } from './remote-controller.interface';
import { IAuthorizeable } from '../behavior/access/authorizable.interface';

import { Policy } from '../model/entity/policy.model';
import { IPolicyAdder } from '../behavior/access/policy-adder.interface';
import { IPolicyReader } from '../behavior/access/policy-reader.interface';


export interface IAccessController extends IRemoteController, IAuthorizeable, IPolicyAdder, IPolicyReader {

}